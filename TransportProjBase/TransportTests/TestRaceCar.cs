﻿using NUnit.Framework;
using TransportProj.Passengers;

namespace TransportProj.Test
{
    [TestFixture]
    class TestRaceCar
    {
        [Test]
        public void Test_MoveLeft_Moves_Two_Spaces_When_Possible()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 0, 0, 0, city);
            var raceCar = new RaceCar(5, 0, city, passenger);

            raceCar.MoveLeft();

            Assert.AreEqual(3, raceCar.XPos);
            Assert.AreEqual(passenger.StartingYPos, raceCar.YPos);
        }

        [Test]
        public void Test_MoveLeft_Moves_One_Space_When_Only_Possibility()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(1, 0, 0, 0, city);
            var raceCar = new RaceCar(1, 0, city, passenger);

            raceCar.MoveLeft();

            Assert.AreEqual(0, raceCar.XPos);
            Assert.AreEqual(passenger.StartingYPos, raceCar.YPos);
        }

        [Test]
        public void Test_MoveLeft_Moves_One_Space_When_Passenger_Is_Next_To_Destination()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 0, 4, 0, city);
            var raceCar = new RaceCar(5, 0, city, passenger);

            raceCar.MoveLeft();

            Assert.AreEqual(4, raceCar.XPos);
            Assert.AreEqual(passenger.StartingYPos, raceCar.YPos);
        }

        [Test]
        public void Test_MoveRight_Moves_Two_Spaces_When_Possible()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 0, 10, 0, city);
            var raceCar = new RaceCar(5, 0, city, passenger);

            raceCar.MoveRight();

            Assert.AreEqual(7, raceCar.XPos);
            Assert.AreEqual(passenger.StartingYPos, raceCar.YPos);
        }

        [Test]
        public void Test_MoveRight_Moves_One_Space_When_Only_Possibility()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(9, 0, 10, 0, city);
            var raceCar = new RaceCar(9, 0, city, passenger);

            raceCar.MoveRight();

            Assert.AreEqual(10, raceCar.XPos);
            Assert.AreEqual(passenger.StartingYPos, raceCar.YPos);
        }

        [Test]
        public void Test_MoveRight_Moves_One_Space_When_Passenger_Is_Next_To_Destination()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 0, 6, 0, city);
            var raceCar = new RaceCar(5, 0, city, passenger);

            raceCar.MoveRight();

            Assert.AreEqual(6, raceCar.XPos);
            Assert.AreEqual(passenger.StartingYPos, raceCar.YPos);
        }

        [Test]
        public void Test_MoveUp_Moves_Two_Spaces_When_Possible()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 0, 10, 10, city);
            var raceCar = new RaceCar(5, 0, city, passenger);

            raceCar.MoveUp();

            Assert.AreEqual(2, raceCar.YPos);
            Assert.AreEqual(passenger.StartingXPos, raceCar.XPos);
        }

        [Test]
        public void Test_MoveUp_Moves_One_Space_When_Only_Possibility()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(9, 9, 10, 10, city);
            var raceCar = new RaceCar(9, 9, city, passenger);

            raceCar.MoveUp();

            Assert.AreEqual(10, raceCar.YPos);
            Assert.AreEqual(passenger.StartingXPos, raceCar.XPos);
        }

        [Test]
        public void Test_MoveUp_Moves_One_Space_When_Passenger_Is_Next_To_Destination()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 7, 6, 8, city);
            var raceCar = new RaceCar(5, 7, city, passenger);

            raceCar.MoveUp();

            Assert.AreEqual(8, raceCar.YPos);
            Assert.AreEqual(passenger.StartingXPos, raceCar.XPos);
        }

        [Test]
        public void Test_MoveDown_Moves_Two_Spaces_When_Possible()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(5, 10, 10, 0, city);
            var raceCar = new RaceCar(5, 10, city, passenger);

            raceCar.MoveDown();

            Assert.AreEqual(8, raceCar.YPos);
            Assert.AreEqual(passenger.StartingXPos, raceCar.XPos);
        }

        [Test]
        public void Test_MoveDown_Moves_One_Space_When_Only_Possibility()
        {
            var city = new City(10, 10);
            var yVal = 1;
            var xVal = 0;
            var passenger = new HurriedPassenger(xVal, yVal, xVal, 0, city);
            var raceCar = new RaceCar(xVal, yVal, city, passenger);

            raceCar.MoveDown();

            Assert.AreEqual(yVal - 1, raceCar.YPos);
            Assert.AreEqual(xVal, raceCar.XPos);
        }

        [Test]
        public void Test_MoveDown_Moves_One_Space_When_Passenger_Is_Next_To_Destination()
        {
            var city = new City(10, 10);
            var passenger = new HurriedPassenger(0, 6, 0, 5, city);
            var raceCar = new RaceCar(0, 6, city, passenger);

            raceCar.MoveDown();

            Assert.AreEqual(5, raceCar.YPos);
            Assert.AreEqual(passenger.StartingXPos, raceCar.XPos);
        }
    }
}
