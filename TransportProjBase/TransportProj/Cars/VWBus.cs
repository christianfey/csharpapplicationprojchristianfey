﻿using System;

namespace TransportProj
{
    public class VWBus : Car
    {
        private Random random
        {
            get; set;
        }

        public VWBus(int xPos, int yPos, City city, Passenger passenger) 
            : base(xPos, yPos, city, passenger)
        {
            random = new Random();
        }

        private bool ShouldSpreadLove()
        {
            if (Passenger == null)
                return false;

            
            if (random.Next(10) > 7)
            {
                Console.WriteLine("The Hippy is spreading their love to everyone around");
                return true;
            }
            return false;
        }

        public override void MoveUp()
        {
            if (YPos < City.YMax && !ShouldSpreadLove())
            {
                YPos++;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (YPos > 0 && !ShouldSpreadLove())
            {
                YPos--;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if (XPos < City.XMax && !ShouldSpreadLove())
            {
                XPos++;
                WritePositionToConsole();
            }
        }

        public override void MoveLeft()
        {
            if (XPos > 0 && !ShouldSpreadLove())
            {
                XPos--;
                WritePositionToConsole();
            }
        }

    }
}
