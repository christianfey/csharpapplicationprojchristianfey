﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    class CarFactory
    {
        private City City { get; set; }
        public CarFactory(City city)
        {
            City = city;
        }

        public Car CreateCar(int xPos, int yPos)
        {
            var passenger = City.LonelyPassenger;

            var carType = passenger.GetCarType();

            return (Car)Activator.CreateInstance(carType, xPos, yPos, City, null);
        }
    }
}
