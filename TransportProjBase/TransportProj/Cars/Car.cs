﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("{0} moved to x - {1} y - {2}", this.GetType().Name, XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public bool HasPassenger()
        {
            return Passenger != null;
        }

        public bool AtDestination()
        {
            if (!HasPassenger())
                return false;

            return (XPos == Passenger.DestinationXPos && YPos == Passenger.DestinationYPos);
        }

        public bool ReadyToPickupPassenger(Passenger passenger)
        {
            return (XPos == passenger.StartingXPos && YPos == passenger.StartingYPos);
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();
    }
}
