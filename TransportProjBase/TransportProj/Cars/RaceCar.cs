﻿namespace TransportProj
{
    public class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger) 
            : base(xPos, yPos, city, passenger)
        {
        }

        public override void MoveUp()
        {
            for (int i = 0; i < 2; i++)
            {
                if (!HasPassenger())
                {
                    if (City.LonelyPassenger.StartingYPos != YPos)
                    {
                        YPos++;
                    }
                    continue;
                }

                if ((YPos < City.YMax) && (YPos < Passenger.DestinationYPos))
                {
                    YPos++;
                }
            }
            WritePositionToConsole();
        }

        public override void MoveDown()
        {
            for (int i = 0; i < 2; i++)
            {
                if (!HasPassenger())
                {
                    if (City.LonelyPassenger.StartingYPos != YPos)
                    {
                        YPos--;
                    }
                    continue;
                }

                if ((YPos > 0) && (YPos > Passenger.DestinationYPos))
                {
                    YPos--;
                }
            }
            WritePositionToConsole();
        }

        public override void MoveRight()
        {
            for (int i = 0; i < 2; i++)
            {
                if (!HasPassenger())
                {
                    if (City.LonelyPassenger.StartingXPos != XPos)
                    {
                        XPos++;
                    }
                    continue;
                }

                if ((XPos < City.XMax) && (XPos < Passenger.DestinationXPos))
                {
                    XPos++;
                }
            }
            WritePositionToConsole();
        }

        public override void MoveLeft()
        {
            for (int i = 0; i < 2; i++)
            {
                if (!HasPassenger())
                {
                    if (City.LonelyPassenger.StartingXPos != XPos)
                    {
                        XPos--;
                    }
                    continue;
                }

                if ((XPos > 0) && (XPos > Passenger.DestinationXPos))
                {
                    XPos--;
                }
            }
            WritePositionToConsole();
        }
    }
}
