﻿using System;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 20;
            int CityWidth = 20;

            City MyCity = new City(CityLength, CityWidth);

            var tries = 10;
            while (tries > 0)
            {
                tries--;
                Passenger passenger = MyCity.AddPassengerToCity(
                    rand.Next(CityLength - 1),
                    rand.Next(CityWidth - 1),
                    rand.Next(CityLength - 1),
                    rand.Next(CityWidth - 1));

                Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

                Console.WriteLine(string.Format("The {0} is at ({1},{2}) and is going to ({3},{4})",
                    passenger.GetPassengerTypeString(),
                    passenger.StartingXPos, passenger.StartingYPos,
                    passenger.DestinationXPos, passenger.DestinationYPos));
                Console.WriteLine(string.Format("The {0} is at ({1},{2})",
                    car.GetType().Name,
                    car.XPos, car.YPos));

                while (!passenger.IsAtDestination())
                {
                    Tick(car, passenger);
                }

                passenger.GetOutOfCar();
                Console.WriteLine();
            }
            // Wait before exiting
            Console.WriteLine("Press any key to continue... (where is the \"any\" key?!)");
            Console.ReadLine();

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            // If the car has a passenger, we need to move or drop them off
            if (car.HasPassenger())
            {
                if (!car.AtDestination())
                {
                    MoveCar(car, passenger, false);
                }
            }
            // If not, we need to move to the passenger or pick them up
            else
            {
                if (car.ReadyToPickupPassenger(passenger))
                {
                    // We have arrived at the passenger
                    passenger.GetInCar(car);
                }
                else
                {
                    MoveCar(car, passenger, true);
                }
            }
        }

        private static void MoveCar(Car car, Passenger passenger, bool isPickup)
        {
            if (car.XPos < (isPickup ? passenger.StartingXPos : passenger.DestinationXPos))
            {
                car.MoveRight();
            }
            else if (car.YPos < (isPickup ? passenger.StartingYPos : passenger.DestinationYPos))
            {
                car.MoveUp();
            }
            else if (car.XPos > (isPickup ? passenger.StartingXPos : passenger.DestinationXPos))
            {
                car.MoveLeft();
            }
            else if (car.YPos > (isPickup ? passenger.StartingYPos : passenger.DestinationYPos))
            {
                car.MoveDown();
            }
            else
            {
                throw new Exception("We have a conundrum and don't know what to do");
            }
        }
    }
}
