﻿
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using TransportProj.Passengers;

namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }
        public Passenger LonelyPassenger { get; set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddCarToCity(int xPos, int yPos)
        {
            var carFactory = new CarFactory(this);
            return carFactory.CreateCar(xPos, yPos);
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            var passengerFactory = new PassengerFactory(this);
            LonelyPassenger = (Passenger)passengerFactory.GetNewPassenger();
            
            return LonelyPassenger;
        }
    }
}
