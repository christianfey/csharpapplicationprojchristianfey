﻿using System;

namespace TransportProj.Passengers
{
    public class HippyPassenger : Passenger
    {
        public HippyPassenger(int startXPos, int startYPos, int destXPos, int destYPos, City city) : 
            base(startXPos, startYPos, destXPos, destYPos, city)
        { }

        public override string GetPassengerTypeString()
        {
            return "Dirty Hippy";
        }

        public override Type GetCarType()
        {
            return typeof(VWBus);
        }

        public override PassengerType GetPassengerType()
        {
            return PassengerType.HIPPY_PASSENGER;
        }
    }
}
