﻿namespace TransportProj
{
    public enum PassengerType
    {
       NORMAL_PASSENGER,
       HURRIED_PASSENGER,
       HIPPY_PASSENGER
    }
}
