﻿using System;
using System.ComponentModel;
using System.Drawing;

namespace TransportProj.Passengers
{
    public class PassengerFactory
    {
        private readonly Random random;
        private City CurrentCity { get; set; }

        public PassengerFactory(City city)
        {
            random = new Random();
            CurrentCity = city;
        }

        public IPassenger GetNewPassenger()
        {
            var passengerType = (PassengerType) random.Next(Enum.GetValues(typeof(PassengerType)).Length);
            var startPos = GetStartPosition();
            var destPos = GetDestination();

            switch (passengerType)
            {
                case PassengerType.NORMAL_PASSENGER:
                    return new NormalPassenger(startPos.X, startPos.Y, destPos.X, destPos.Y, CurrentCity);
                case PassengerType.HIPPY_PASSENGER:
                    return new HippyPassenger(startPos.X, startPos.Y, destPos.X, destPos.Y, CurrentCity);
                case PassengerType.HURRIED_PASSENGER:
                    return new HurriedPassenger(startPos.X, startPos.Y, destPos.X, destPos.Y, CurrentCity);
                default:
                    throw new InvalidEnumArgumentException("Invalid passenger type detected");
            }
        }

        private Point GetStartPosition()
        {
            var point = new Point
            {
                X = random.Next(City.XMax),
                Y = random.Next(City.YMax)
            };

            return point;
        }

        private Point GetDestination()
        {
            // This could have different logic, but since it's random anyways, not point reinventing the wheel
            return GetStartPosition();
        }
    }
}
