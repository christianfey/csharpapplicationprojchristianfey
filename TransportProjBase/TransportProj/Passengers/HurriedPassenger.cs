﻿using System;

namespace TransportProj.Passengers
{
    public class HurriedPassenger : Passenger
    {
        public HurriedPassenger(int startXPos, int startYPos, int destXPos, int destYPos, City city) : 
            base(startXPos, startYPos, destXPos, destYPos, city)
        {
        }

        public override string GetPassengerTypeString()
        {
            return "Speed Racer";
        }

        public override Type GetCarType()
        {
            return typeof(RaceCar);
        }

        public override PassengerType GetPassengerType()
        {
            return PassengerType.HURRIED_PASSENGER;
        }
    }
}
