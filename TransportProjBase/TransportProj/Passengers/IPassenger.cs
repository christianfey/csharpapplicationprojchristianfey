﻿using System;

namespace TransportProj.Passengers
{
    public interface IPassenger
    {
        PassengerType GetPassengerType();
        string GetPassengerTypeString();
        Type GetCarType();
    }
}
