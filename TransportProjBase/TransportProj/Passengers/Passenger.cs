﻿using System;
using TransportProj.Passengers;

namespace TransportProj
{
    public abstract class Passenger : IPassenger
    {
        public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public Car Car { get; set; }
        public City City { get; private set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
        }

        public abstract string GetPassengerTypeString();
        public abstract PassengerType GetPassengerType();
        public abstract Type GetCarType();
    
        public void GetInCar(Car car)
        {
            Car = car;
            car.PickupPassenger(this);
            Console.WriteLine(string.Format("The {0} got in car.", GetPassengerTypeString()));
        }

        public void GetOutOfCar()
        {
            Car = null;
            Console.WriteLine(string.Format("The {0} got out of the car.", GetPassengerTypeString()));
        }

        public int GetCurrentXPos()
        {
            if(Car == null)
            {
                return StartingXPos;
            }
            else
            {
                return Car.XPos;
            }
        }

        public int GetCurrentYPos()
        {
            if (Car == null)
            {
                return StartingYPos;
            }
            else
            {
                return Car.YPos;
            }
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
        }
    }
}
