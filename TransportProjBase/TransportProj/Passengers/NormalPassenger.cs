﻿using System;

namespace TransportProj.Passengers
{
    public class NormalPassenger : Passenger
    {
        public NormalPassenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
            : base(startXPos, startYPos, destXPos, destYPos, city)
        { }

        public override string GetPassengerTypeString()
        {
            return "Normal Passenger";
        }

        public override Type GetCarType()
        {
            return typeof(Sedan);
        }

        public override PassengerType GetPassengerType()
        {
            return PassengerType.NORMAL_PASSENGER;
        }
    }
}
